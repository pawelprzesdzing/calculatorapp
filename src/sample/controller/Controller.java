package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    Float firstNumber = 0f;
    int operation = -1;

    //buttons
    @FXML
    private Button clearButton;
    @FXML
    private Button sumButton;
    @FXML
    private Button subtractionButton;
    @FXML
    private Button multiplyButton;
    @FXML
    private Button divideButton;
    @FXML
    private Button oneButton;
    @FXML
    private Button twoButton;
    @FXML
    private Button threeButton;
    @FXML
    private Button fourButton;
    @FXML
    private Button fiveButton;
    @FXML
    private Button sixButton;
    @FXML
    private Button sevenButton;
    @FXML
    private Button eightButton;
    @FXML
    private Button nineButton;
    @FXML
    private Button zeroButton;
    @FXML
    private Button resultButton;
    @FXML
    private Button powerOfNumber;
    @FXML
    private Button elementOfNumber;
    @FXML
    private TextField textField;

    //action
    @FXML
    public void buttonAction(ActionEvent actionEvent) {
        if (actionEvent.getSource() == oneButton) {
            textField.setText(textField.getText() + "1");
        } else if (actionEvent.getSource() == twoButton) {
            textField.setText(textField.getText() + "2");
        } else if (actionEvent.getSource() == threeButton) {
            textField.setText(textField.getText() + "3");
        } else if (actionEvent.getSource() == fourButton) {
            textField.setText(textField.getText() + "4");
        } else if (actionEvent.getSource() == fiveButton) {
            textField.setText(textField.getText() + "5");
        } else if (actionEvent.getSource() == sixButton) {
            textField.setText(textField.getText() + "6");
        } else if (actionEvent.getSource() == sevenButton) {
            textField.setText(textField.getText() + "7");
        } else if (actionEvent.getSource() == eightButton) {
            textField.setText(textField.getText() + "8");
        } else if (actionEvent.getSource() == nineButton) {
            textField.setText(textField.getText() + "9");
        } else if (actionEvent.getSource() == zeroButton) {
            textField.setText(textField.getText() + "0");
        } else if (actionEvent.getSource() == clearButton) {
            textField.clear();
        } else if (actionEvent.getSource() == powerOfNumber) {
            double pow = Math.pow(Double.parseDouble(textField.getText()), 2);
            textField.setText(String.valueOf(pow));
        } else if (actionEvent.getSource() == elementOfNumber){
            double sqrt = Math.sqrt(Double.parseDouble(textField.getText()));
            textField.setText(String.valueOf(sqrt));
        } else if (actionEvent.getSource() == sumButton) {
            firstNumber = Float.parseFloat(textField.getText());
            operation = 1;
            textField.clear();
        } else if (actionEvent.getSource() == subtractionButton) {
            firstNumber = Float.parseFloat(textField.getText());
            operation = 2;
            textField.clear();
        } else if (actionEvent.getSource() == multiplyButton) {
            firstNumber = Float.parseFloat(textField.getText());
            operation = 3;
            textField.clear();
        } else if (actionEvent.getSource() == divideButton) {
            firstNumber = Float.parseFloat(textField.getText());
            operation = 4;
            textField.clear();
        } else if (actionEvent.getSource() == resultButton){
            Float secondNumber = Float.parseFloat(textField.getText());
            switch (operation){
                case 1:
                    Float res = firstNumber + secondNumber;
                    textField.setText(String.valueOf(res));
                    break;
                case 2:
                    res = firstNumber - secondNumber;
                    textField.setText(String.valueOf(res));
                    break;
                case 3:
                    res = firstNumber * secondNumber;
                    textField.setText(String.valueOf(res));
                    break;
                case 4:
                    res = 0f;
                    try {
                        res = firstNumber / secondNumber;
                    } catch (Exception e){
                        textField.setText("Error");
                    }
                    textField.setText(String.valueOf(res));
                    break;
            }
        }
    }
}
